pub fn quad_rect1<T: Copy>(f: fn(f64, T) -> f64, params: T, a: f64, b: f64,
                       dx: f64) -> f64 {
    let mut sum: f64 = 0.0;
    let dx = if dx * (b - a) < 0.0 {-dx} else {dx};
    let mut x = a;

    while (b - x).abs() > dx.abs() {
        sum += f(x, params) * dx;
        x += dx;
    }
    sum + f(x, params) * (b - x)
}

pub fn quad_rect2<T: Copy>(f: fn(f64, T) -> f64, params: T, a: f64, b: f64,
                       dx: f64) -> f64 {
    let mut sum: f64 = 0.0;
    let dx = if dx * (b - a) < 0.0 {-dx} else {dx};
    let mut x = a + 0.5 * dx;

    while (b - x).abs() > dx.abs() {
        sum += f(x, params) * dx;
        x += dx;
    }

    let x1 = x - 0.5 * dx;
    x = x1 + 0.5 * (b - x1);
    sum + f(x, params) * (b - x1)
}

pub fn quad_simson3<T: Copy>(f: fn(f64, T) -> f64, params: T, a: f64, b: f64,
                        dx: f64) -> f64 {
    let mut sum: f64 = 0.0;
    let dx = if dx * (b - a) < 0.0 {-dx} else {dx};
    let k = 0.5 * dx;
    let mut x = a;

    while (b - x).abs() > dx.abs() {
        let c1 = 1.0/3.0 * k * f(x, params);
        let c2 = 4.0/3.0 * k * f(x + 0.5*dx, params);
        let c3 = 1.0/3.0 * k * f(x + dx, params);
        sum += c1 + c2 + c3;
        x += dx;
    }
    let k = 0.5 * (b - x);
    let c1 = 1.0/3.0 * k * f(x, params);
    let c2 = 4.0/3.0 * k * f(x + 0.5*(b - x), params);
    let c3 = 1.0/3.0 * k * f(b, params);

    sum + c1 + c2 + c3
}

pub fn quad_simson5<T: Copy>(f: fn(f64, T) -> f64, params: T, a: f64, b: f64,
                        dx: f64) -> f64 {
    let mut sum: f64 = 0.0;
    let dx = if dx * (b - a) < 0.0 {-dx} else {dx};
    let k = 0.5 * dx;
    let mut x = a;

    while (b - x).abs() > dx.abs() {
        let c1 = 7.0/45.0 * k * f(x, params);
        let c2 = 32.0/45.0 * k * f(x + 0.25*dx, params);
        let c3 = 4.0/15.0 * k * f(x + 0.5*dx, params);
        let c4 = 32.0/45.0 * k * f(x + 0.75*dx, params);
        let c5 = 7.0/45.0 * k * f(x + dx, params);
        sum += c1 + c2 + c3 + c4 + c5;
        x += dx;
    }
    let k = 0.5 * (b - x);
    let c1 = 7.0/45.0 * k * f(x, params);
    let c2 = 32.0/45.0 * k * f(x + 0.25*(b - x), params);
    let c3 = 4.0/15.0 * k * f(x + 0.5*(b - x), params);
    let c4 = 32.0/45.0 * k * f(x + 0.75*(b - x), params);
    let c5 = 7.0/45.0 * k * f(b, params);

    sum + c1 + c2 + c3 + c4 + c5
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
    }
}
