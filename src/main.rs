use std::f64::consts;

extern crate librquad;
use librquad::*;

fn main() {
    const DX: f64 = 0.10;
    const DX1: f64 = 0.1031;
    const A: f64 = 2.0;
    let mut x: f64 = 0.0;
    let fun: fn(f64, f64) -> f64 = f1;
    let rect1: f64 = 0.0;
    let rect2: f64 = 0.0;
    let simson3: f64 = 0.0;
    let simson5: f64 = 0.0;
    while x < 2.0 * consts::PI {
        let x0 = x;
        if x + DX < 2.0 * consts::PI {
            x += DX;
        } else {
            x = 2.0 * consts::PI;
        }
        let rect1 = rect1 + quad_rect1(fun, A, x0, x, DX);
        let rect2 = rect2 + quad_rect2(fun, A, x0, x, DX);
        let simson3 = simson3 + quad_simson3(fun, A, x0, x, DX);
        let simson5 = simson5 + quad_simson5(fun, A, x0, x, DX);
//        let val: f64 = - x.powi(3) / 3.0 + x.powi(5) / 5.0;
        let val: f64 = A * x.sin();
        print!("{:5.2} {:11.5}", x, val);

        let sums = [rect1, rect2, simson3, simson5];
        for i in 0..sums.len() {
            let sum = sums[i];
            print!("{:11.5} {:15.5e} ", sum, ((val - sum) / val).abs());
        }
        print!("\n");
    }
}

fn f1(x: f64, a: f64) -> f64 {
    a * x.cos()
}

fn f2(x: f64, a: f64) -> f64 {
    -x.powi(2) + x.powi(4)
}

